2018-05-20 15:42:48.453
Application version: 3.2.70.0

http://master:8096/emby/videos/a47886f923dc363b69dd98353d56fec0/hls1/main/0.ts?DeviceId=8284865427a4b25a7c6601d12cb1e1ceceff4401&MediaSourceId=a47886f923dc363b69dd98353d56fec0&VideoCodec=h264&AudioCodec=aac&AudioStreamIndex=1&VideoBitrate=139871878&AudioBitrate=128122&PlaySessionId=df038be1556142babd477285580fc9d4&api_key=994be0587364471da135041b5ec19c90&CopyTimestamps=false&TranscodingMaxAudioChannels=2&EnableSubtitlesInManifest=false&Tag=e03b0f212bfc487639df205d681cfcdb&RequireAvc=true&RequireNonAnamorphic=false&SegmentContainer=ts&MinSegments=1&BreakOnNonKeyFrames=True&TranscodeReasons=ContainerNotSupported,VideoCodecNotSupported,AudioCodecNotSupported&h264-profile=high,main,baseline,constrainedbaseline&h264-level=51&h264-deinterlace=true

{"Protocol":"File","Id":"a47886f923dc363b69dd98353d56fec0","Path":"/mnt/share1/ocean-avi.avi","Type":"Default","Container":"avi","Name":"ocean-avi.avi","IsRemote":false,"ETag":"e03b0f212bfc487639df205d681cfcdb","RunTimeTicks":252720000,"ReadAtNativeFramerate":false,"IgnoreDts":false,"IgnoreIndex":false,"GenPtsInput":false,"SupportsTranscoding":true,"SupportsDirectStream":true,"SupportsDirectPlay":true,"IsInfiniteStream":false,"RequiresOpening":false,"RequiresClosing":false,"RequiresLooping":false,"SupportsProbing":true,"VideoType":"VideoFile","MediaStreams":[{"Codec":"mpeg4","CodecTag":"xvid","TimeBase":"1001/30000","CodecTimeBase":"1001/30000","DisplayTitle":"1080P MPEG4","IsInterlaced":false,"BitRate":1645248,"RefFrames":1,"IsDefault":false,"IsForced":false,"Height":1080,"Width":1920,"AverageFrameRate":29.97003,"RealFrameRate":29.97003,"Profile":"Simple Profile","Type":"Video","AspectRatio":"16:9","Index":0,"IsExternal":false,"IsTextSubtitleStream":false,"SupportsExternalStream":false,"PixelFormat":"yuv420p","Level":3,"IsAnamorphic":false},{"Codec":"mp3","TimeBase":"3/125","CodecTimeBase":"1/48000","DisplayTitle":"MP3 stereo","IsInterlaced":false,"ChannelLayout":"stereo","BitRate":128122,"Channels":2,"SampleRate":48000,"IsDefault":false,"IsForced":false,"Type":"Audio","Index":1,"IsExternal":false,"IsTextSubtitleStream":false,"SupportsExternalStream":false,"Level":0,"IsAnamorphic":false}],"Formats":[],"Bitrate":1785059,"RequiredHttpHeaders":{}}

User policy for test: EnablePlaybackRemuxing: True EnableVideoPlaybackTranscoding: True EnableAudioPlaybackTranscoding: True

/opt/emby-server/bin/ffmpeg -f avi -i file:"/mnt/share1/ocean-avi.avi" -threads 0 -map 0:0 -map 0:1 -map -0:s -codec:v:0 libx264 -vf "scale=trunc(min(max(iw\,ih*dar)\,1920)/2)*2:trunc(ow/dar/2)*2" -pix_fmt yuv420p -preset veryfast -crf 23 -maxrate 4113120 -bufsize 8226240 -profile:v high -level 4.1 -x264opts:0 subme=0:me_range=4:rc_lookahead=10:me=dia:no_chroma_me:8x8dct=0:partitions=none -force_key_frames "expr:if(isnan(prev_forced_t),eq(t,t),gte(t,prev_forced_t+3))" -copyts -vsync -1 -codec:a:0 aac -strict experimental -ac 2 -ab 128122  -f segment -max_delay 5000000 -avoid_negative_ts disabled -map_metadata -1 -map_chapters -1 -start_at_zero -segment_time 3  -individual_header_trailer 0 -segment_format mpegts -segment_list_type m3u8 -segment_start_number 0 -segment_list "/var/lib/emby-server/transcoding-temp/f1889096bbf68ccf6ea5f84301712710.m3u8" -y "/var/lib/emby-server/transcoding-temp/f1889096bbf68ccf6ea5f84301712710%d.ts"

ffmpeg version 3.2.10-1~deb9u1~bpo8+1 Copyright (c) 2000-2018 the FFmpeg developers
  built with gcc 4.9.2 (Debian 4.9.2-10)
  configuration: --prefix=/usr --extra-version='1~deb9u1~bpo8+1' --toolchain=hardened --libdir=/usr/lib/x86_64-linux-gnu --incdir=/usr/include/x86_64-linux-gnu --enable-gpl --disable-stripping --enable-avresample --enable-avisynth --enable-gnutls --enable-ladspa --enable-libass --enable-libbluray --enable-libbs2b --enable-libcaca --enable-libcdio --disable-libebur128 --enable-libflite --enable-libfontconfig --enable-libfreetype --enable-libfribidi --enable-libgme --enable-libgsm --enable-libmodplug --enable-libmp3lame --enable-libopenjpeg --enable-libopus --enable-libpulse --enable-librubberband --enable-libshine --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libssh --enable-libtheora --enable-libtwolame --enable-libvorbis --enable-libvpx --enable-libwavpack --enable-libwebp --enable-libx265 --enable-libxvid --enable-libzmq --enable-libzvbi --enable-omx --enable-openal --enable-opengl --enable-sdl2 --enable-libdc1394 --enable-libiec61883 --enable-chromaprint --enable-frei0r --enable-libopencv --enable-libx264 --enable-shared
  libavutil      55. 34.101 / 55. 34.101
  libavcodec     57. 64.101 / 57. 64.101
  libavformat    57. 56.101 / 57. 56.101
  libavdevice    57.  1.100 / 57.  1.100
  libavfilter     6. 65.100 /  6. 65.100
  libavresample   3.  1.  0 /  3.  1.  0
  libswscale      4.  2.100 /  4.  2.100
  libswresample   2.  3.100 /  2.  3.100
  libpostproc    54.  1.100 / 54.  1.100

Input #0, avi, from 'pipe:0':
  Metadata:
    encoder         : Lavf56.40.101
  Duration: 00:00:25.27, start: 0.000000, bitrate: N/A
    Stream #0:0: Video: mpeg4 (Simple Profile) (xvid / 0x64697678), yuv420p, 1920x1080 [SAR 1:1 DAR 16:9], 29.97 fps, 29.97 tbr, 29.97 tbn, 29.97 tbc

    Stream #0:1: Audio: mp3 (U[0][0][0] / 0x0055), 48000 Hz, stereo, s16p, 128 kb/s
[libx264 @ 0x5638c8d47900] using SAR=1/1
[libx264 @ 0x5638c8d47900] using cpu capabilities: MMX2 SSE2Fast SSSE3 SSE4.2 AVX AVX2 FMA3 LZCNT BMI2

[libx264 @ 0x5638c8d47900] 
profile Main, level 4.1
[libx264 @ 0x5638c8d47900] 264 - core 142 r2431 a5831aa - H.264/MPEG-4 AVC codec - Copyleft 2003-2014 - http://www.videolan.org/x264.html - options: cabac=1 ref=1 deblock=1:0:0 analyse=0x1:0 me=dia subme=0 psy=1 psy_rd=1.00:0.00 mixed_ref=0 me_range=4 chroma_me=0 trellis=0 8x8dct=0 cqm=0 deadzone=21,11 fast_pskip=1 chroma_qp_offset=0 threads=6 lookahead_threads=1 sliced_threads=0 nr=0 decimate=1 interlaced=0 bluray_compat=0 constrained_intra=0 bframes=3 b_pyramid=2 b_adapt=1 b_bias=0 direct=1 weightb=1 open_gop=0 weightp=1 keyint=250 keyint_min=25 scenecut=40 intra_refresh=0 rc_lookahead=10 rc=crf mbtree=1 crf=23.0 qcomp=0.60 qpmin=0 qpmax=69 qpstep=4 vbv_maxrate=4113 vbv_bufsize=8226 crf_max=0.0 nal_hrd=none filler=0 ip_ratio=1.40 aq=1:1.00
[segment @ 0x5638c8d46300] Opening '/var/lib/dtms/transcoding-temp/f1889096bbf68ccf6ea5f843017127100.ts' for writing
Output #0, segment, to '/var/lib/dtms/transcoding-temp/f1889096bbf68ccf6ea5f84301712710%d.ts':
  Metadata:
    encoder         : Lavf57.56.101
    Stream #0:0: Video: h264 (libx264), yuv420p, 1920x1080 [SAR 1:1 DAR 16:9], q=-1--1, 29.97 fps, 90k tbn, 29.97 tbc
    Metadata:
      encoder         : Lavc57.64.101 libx264
    Side data:
      cpb: bitrate max/min/avg: 4113000/0/0 buffer size: 8226000 vbv_delay: -1
    Stream #0:1: Audio: aac (LC), 48000 Hz, stereo, fltp, 128 kb/s
    Metadata:
      encoder         : Lavc57.64.101 aac
Stream mapping:
  Stream #0:0 -> #0:0 (mpeg4 (native) -> h264 (libx264))
  Stream #0:1 -> #0:1 (mp3 (native) -> aac (native))

frame=   21 fps=3.2 q=29.0 size=N/A time=00:00:00.76 bitrate=N/A dup=1 drop=0 speed=0.116x    
frame=   23 fps=1.9 q=29.0 size=N/A time=00:00:00.83 bitrate=N/A dup=1 drop=0 speed=0.0677x    