import json
import logging
import pathlib
import tempfile
from http import HTTPStatus
from queue import Queue
from shutil import copyfileobj
from threading import Thread

from flask import Flask, request, abort, make_response
from twisted.internet import reactor

from dtms.worker import transcode, ffmpeg_arguments
from dtms.worker.config import get as get_config

config = get_config()

app = Flask(__name__)

logger = logging.getLogger('api')
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler(config['log_file'], 'w')
file_handler.setLevel(logging.DEBUG)

formatter = logging.Formatter(
    fmt='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
)

file_handler.setFormatter(formatter)

logger.addHandler(file_handler)
app.logger.addHandler(file_handler)

pathlib.Path(config['transcoding_dir']).mkdir(parents=True, exist_ok=True)

session_list = []
transcoding_queue = Queue(maxsize=1)


@app.route('/health', methods=['GET'])
def status():
    return json.dumps({'status': 'up'})


@app.route('/transcode-sessions', methods=['POST'])
def new_transcode_session():
    json_data = request.get_json()

    # if a KeyError is raised, Flask responds with HTTP 400
    ffmpeg_args = json_data['args']
    id_ = json_data['id']
    master_transcoding_dir = json_data['master_transcoding_dir']

    if next((s for s in session_list if s['id'] == id_), None):
        return f'Session with id: {id_} already exists', HTTPStatus.UNPROCESSABLE_ENTITY

    transcoder_args = ffmpeg_arguments.process(
        config['transcoding_dir'],
        ffmpeg_args,
        id_
    )

    transcoder = transcode.Transcoder(
        transcoder_args,
        config['transcoding_dir'],
        config['master']['api']['address'],
        master_transcoding_dir
    )

    session = {
        'id': id_,
        'transcoder': transcoder,
        'cancelled': False
    }

    session_list.append(session)

    logger.info(f'New session created and added to queue, id: {id_}')
    #logger.debug(f'original args: {ffmpeg_args}')
    #logger.debug(f'adjusted args: {transcoder_args}')

    headers = {'Location': f'{request.path}/{session["id"]}'}
    return make_response(('New session created', HTTPStatus.CREATED, headers))


@app.route('/transcode-sessions/<id_>/original-file', methods=['POST'])
def file_upload(id_):
    session = next((s for s in session_list if s['id'] == id_), None)
    if session:
        request_file_storage = next(request.files.values(), None)

        if request_file_storage:
            # original file provided by Flask is closed when request completes
            # make a copy here to be used in a new thread
            original_file_copy = tempfile.SpooledTemporaryFile()
            copyfileobj(request_file_storage.stream, original_file_copy)

            session['original_file'] = original_file_copy
            logger.debug(f'file received for id: {session["id"]}, tell(): {original_file_copy.tell()}')

            Thread(target=join_queue_and_put, args=(session,)).start()

            logger.info(f'Transcoding scheduled for id {id_}')

            return 'Transcoding scheduled', HTTPStatus.CREATED
        else:
            return 'A file must be present in request body', HTTPStatus.BAD_REQUEST
    else:
        abort(HTTPStatus.NOT_FOUND)


@app.route('/transcode-sessions/<id_>', methods=['DELETE'])
def stop_session(id_):
    session = next((s for s in session_list if s['id'] == id_), None)
    if session:
        transcoder = session['transcoder']
        transcoder.stop()  # Transcoder process finished callback is Queue.task_done()

        session['cancelled'] = True  # queue won't process item in future

        msg = f'Session cancelled, id: {id_}'

        logger.info(msg)
        return msg, HTTPStatus.ACCEPTED
    else:
        abort(HTTPStatus.NOT_FOUND)


def join_queue_and_put(session):
    transcoding_queue.join()
    transcoding_queue.put(session)
    logger.debug(f'session: {session["id"]} added to queue')


def start_transcoding(session, finished_callback):
    transcoder = session['transcoder']
    transcoder.start(session['id'], session['original_file'], finished_callback)
    logger.info(f'Transcoding started for id {session["id"]}')


def transcoding_loop():
    logger.debug('transcoding queue loop started')
    finished_callback = transcoding_queue.task_done
    try:
        while True:
            session = transcoding_queue.get()
            if not session['cancelled']:
                start_transcoding(session, finished_callback)
            else:
                finished_callback()
    except:
        logger.exception('Exception caught in transcoding queue loop:', exc_info=1)


Thread(target=reactor.run, args=(False,)).start()
Thread(target=transcoding_loop).start()
