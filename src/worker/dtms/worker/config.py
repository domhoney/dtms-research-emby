import os.path
import anyconfig

default_bind_host = '0.0.0.0'

DEFAULT_CONFIG = {
    'master': {
        'api': {
            'address': 'http://master:8000'
        }
    },
    'transcoding_dir': '/var/lib/dtms/transcoding-temp',
    'log_file': '/var/log/dtms/worker/api.log'
}

conf_file = '/etc/dtms/worker.yaml'


class Loader(object):
    _config = None

    def get(self, ):
        if not self._config:
            self._config = DEFAULT_CONFIG.copy()

            if os.path.exists(conf_file):
                file_config = anyconfig.load(conf_file)
                anyconfig.merge(self._config, file_config)

            master_api_address = os.environ.get("DTMS_MASTER_API_ADDRESS", None) or DEFAULT_CONFIG["master"]["api"]["address"]

            self._config['master']['api']['address'] = master_api_address

        return self._config


config_loader = Loader()


def get():
    return config_loader.get()
