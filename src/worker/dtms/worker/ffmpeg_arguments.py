import os.path


def process(transcoding_base_dir, ffmpeg_args, id_):
    new_args = list(ffmpeg_args)

    for i, arg in enumerate(new_args):
        if '-i' == arg:
            """Replace input filepath arg with ffmpeg's stdin mechanism.
            This allows dtms to pipe the spooled temporary video file to ffmpeg"""
            new_args[i + 1] = 'pipe:0'
        elif id_ in arg:
            new_args[i] = adjust_path(transcoding_base_dir, arg)

    return new_args


def adjust_path(transcoding_base_dir, path):
    basename = os.path.basename(path)
    return os.path.join(transcoding_base_dir, basename)
