import logging
import os.path
import json
import time

import requests
from watchdog.events import FileSystemEventHandler, FileCreatedEvent, FileMovedEvent
from watchdog.observers import Observer

logger = logging.getLogger('watch')
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler('/var/log/dtms/worker/watch.log', 'w')
file_handler.setLevel(logging.DEBUG)

formatter = logging.Formatter(
    fmt='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
)

file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


def get_basename(file_path):
    return os.path.basename(file_path)


def get_filename(basename):
    return basename.rsplit('.', 1)[0]


class SegmentCreationHandler(FileSystemEventHandler):
    """
    Monitors fs events according to ffmpeg output.
    3 kinds of files exist in the transcoding dir during processing:
        segment, segment_list, segment_list_tmp
    Order of events when ffmpeg outputs segments:
        CREATE segment
        MODIFY segment * X
        CREATE segment_list_tmp
        MODIFY segment_list_tmp
        MOVE segment_list_tmp -> segment_list
        MODIFY segment
        repeat
    This class' behaviour is based on the above event observations.
    """
    file_no = 2
    current_segment_files = []
    next_segment_file_created = False

    def __init__(self, id_, master_address, master_transcoding_dir):
        self.id_ = id_
        self.master_address = master_address
        self.master_transcoding_dir = master_transcoding_dir

    def dispatch(self, event):
        try:
            super(SegmentCreationHandler, self).dispatch(event)
        except:
            logger.exception('SegmentCreationHandler exception:', exc_info=1)
            raise

    def on_created(self, event):
        if isinstance(event, FileCreatedEvent):
            src_path = event.src_path
            if self.id_ in src_path:
                if not src_path.endswith('tmp'):
                    if len(self.current_segment_files) == self.file_no:
                        # if this is the segment in the next iteration,
                        # it's time to send the current file buffer
                        self.send_segment_files()
                    self.current_segment_files.append({
                        'path': src_path,
                        'file': None
                    })
                    logger.debug(f'segment captured: {src_path}')

    def on_moved(self, event):
        if isinstance(event, FileMovedEvent):
            src_path = event.src_path
            dest_path = event.dest_path
            if self.id_ in dest_path and src_path.endswith('tmp'):
                self.current_segment_files.append({
                    'path': dest_path,
                    'file': open(dest_path, mode='rb')
                })
                logger.debug(f'segment_list captured: {dest_path}')

    def send_segment_files(self):
        if len(self.current_segment_files) == self.file_no:
            segment_dict = self.current_segment_files[0]
            segment_path = segment_dict['path']
            segment_name = get_basename(segment_path)
            segment_dict_file = segment_dict['file']
            if segment_dict_file:
                segment_file = segment_dict_file
            else:
                segment_file = open(segment_path, mode='rb')

            segment_list_dict = self.current_segment_files[1]
            segment_list_path = segment_list_dict['path']
            segment_list_name = get_basename(segment_list_path)
            segment_list_dict_file = segment_list_dict['file']
            if segment_list_dict_file:
                segment_list_file = segment_list_dict_file
            else:
                segment_list_file = open(segment_list_path, mode='rb')

            logger.info('Sending segment files to worker...')

            extra_data = {'master_transcoding_dir': self.master_transcoding_dir}

            logger.debug(str(extra_data))

            files = {
                'segment': (segment_name, segment_file, 'application/octet-stream'),
                'segment_list': (segment_list_name, segment_list_file, 'application/octet-stream'),
                'json': (None, json.dumps(extra_data), 'application/json')
            }

            new_segment_response = requests.post(
                f'{self.master_address}/transcode-sessions/{self.id_}/segments',
                files=files
            )

            new_segment_response.raise_for_status()

            logger.info('Video file sent to worker')

            segment_file.close()
            segment_list_file.close()

            self.current_segment_files.clear()


class SegmentWatcher(object):
    def __init__(self, id_, transcoding_dir, master_address, master_transcoding_dir):
        self.transcoding_dir = transcoding_dir
        self.observer = Observer()
        self.handler = SegmentCreationHandler(id_, master_address, master_transcoding_dir)

    def start(self):
        self.observer.schedule(self.handler, self.transcoding_dir, recursive=False)
        self.observer.start()
        logger.info('SegmentWatcher started')

    def stop(self):
        self.observer.stop()
        logger.info('SegmentWatcher stopped')

    def transcoding_finished(self):
        # send remaining captured files
        self.handler.send_segment_files()
        time.sleep(10)
        self.stop()
