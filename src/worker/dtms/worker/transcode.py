import requests
import logging
from twisted.internet import protocol, reactor

from dtms.worker import watch

logger = logging.getLogger('transcode')
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler('/var/log/dtms/worker/transcode.log', 'w')
file_handler.setLevel(logging.DEBUG)

formatter = logging.Formatter(
    fmt='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
)

file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class FfmpegProcessProtocol(protocol.ProcessProtocol):
    def __init__(self, id_, input_file, master_address):
        self._id = id_
        self._input_file = input_file
        self.master_address = master_address
        self._finished_callbacks = []

    def add_finished_callbacks(self, callbacks):
        for callback in callbacks:
            if callable(callback):
                self._finished_callbacks.append(callback)

    def connectionMade(self):
        logger.debug(f'writing file to ffmpeg input stream')
        self._input_file.seek(0)
        self.transport.write(self._input_file.read())
        self.transport.closeStdin()
        logger.debug('closed process transport and file')

    def outReceived(self, data):
        self._send_stream_data('stdout', data)

    def errReceived(self, data):
        self._send_stream_data('stderr', data)

    def _send_stream_data(self, stream, data):
        logger.debug(f'Sending {stream} to master: {str(data)}')

        if isinstance(data, bytes):
            data = data.decode('utf-8')

        r = requests.post(
            f'{self.master_address}/transcode-sessions/{self._id}/{stream}',
            json={'output': data}
        )

        r.raise_for_status()

    def processEnded(self, status):
        logger.info('ffmpeg finished, sending delete request to master')
        requests.delete(
            f'{self.master_address}/transcode-sessions/{self._id}'
        )

        for callback in self._finished_callbacks:
            callback()

    def stop(self):
        self.transport.signalProcess('KILL')


class Transcoder(object):
    def __init__(self, transcoder_args, transcoding_dir, master_address, master_transcoding_dir):
        self.transcoder_args = transcoder_args
        self.transcoding_dir = transcoding_dir
        self.master_address = master_address
        self.master_transcoding_dir = master_transcoding_dir
        self.process = None
        self.segment_watcher = None

    def start(self, id_, input_file, finished_callback):
        logger.debug(f'Spawning transcoder, tell(): {input_file.tell()}')
        logger.debug(f'transcoder args: {self.transcoder_args}')

        self.process = FfmpegProcessProtocol(
            id_,
            input_file,
            self.master_address
        )
        self.process.add_finished_callbacks([
            self.finished_callback,
            finished_callback
        ])

        reactor.spawnProcess(self.process, 'ffmpeg', ['ffmpeg'] + self.transcoder_args)

        self.segment_watcher = watch.SegmentWatcher(
            id_,
            self.transcoding_dir,
            self.master_address,
            self.master_transcoding_dir
        )
        self.segment_watcher.start()

    def stop(self):
        if self.process:
            self.process.stop()
        if self.segment_watcher:
            self.segment_watcher.stop()

    def finished_callback(self):
        if self.segment_watcher:
            self.segment_watcher.transcoding_finished()
