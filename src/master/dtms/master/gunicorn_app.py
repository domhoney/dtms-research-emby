import argparse

import gunicorn  #
import gunicorn.glogging  # needed for PyInstaller
import gunicorn.workers.sync  #
import gunicorn.app.base
from gunicorn.config import get_default_config_file

from dtms.master.api import create_app


class GunicornFlaskApp(gunicorn.app.base.Application):
    def __init__(self, app, args=None):
        self.app = app
        self.args = args or []
        super().__init__()

    def load_config(self):
        """Adjusted copy of gunicorn.app.base.Application.load_config
        Steal with pride"""
        parser = self.cfg.parser()
        args = parser.parse_args(args=self.args)

        if args.config:
            self.load_config_from_file(args.config)
        else:
            default_config = get_default_config_file()
            if default_config is not None:
                self.load_config_from_file(default_config)

        # Load up environment configuration
        env_vars = self.cfg.get_cmd_args_from_env()
        if env_vars:
            env_args = parser.parse_args(env_vars)
            for k, v in vars(env_args).items():
                if v is None:
                    continue
                if k == "args":
                    continue
                self.cfg.set(k.lower(), v)

        # Lastly, update the configuration with any command line
        # settings.
        for k, v in vars(args).items():
            if v is None:
                continue
            if k == "args":
                continue
            self.cfg.set(k.lower(), v)

    def load(self):
        return self.app

    def init(self, parser, opts, args):
        pass


if __name__ == '__main__':
    dtms_parser = argparse.ArgumentParser(allow_abbrev=False)
    dtms_parser.add_argument('--broker-address', help='host:port')
    dtms_parser.add_argument('--log-file', help='Path to file for logging')
    api_parsed_args, gunicorn_args = dtms_parser.parse_known_args()
    GunicornFlaskApp(create_app(api_parsed_args), gunicorn_args).run()
