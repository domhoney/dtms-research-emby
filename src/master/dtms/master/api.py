import io
import json
import logging
import os
from http import HTTPStatus
from multiprocessing.connection import Client
import shutil

from flask import Flask, request

from dtms.master.config import get as get_config


def create_app(parsed_args):
    config = get_config(parsed_args)

    app = Flask(__name__)

    logger = logging.getLogger('api')
    logger.setLevel(logging.DEBUG)

    file_handler = logging.FileHandler(config['log_file'], 'w')
    file_handler.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        fmt='%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
    )

    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
    app.logger.addHandler(file_handler)

    class BrokerClient(object):
        def __init__(self, address, authkey):
            self.address = address
            self._authkey = authkey
            self.connection = Client(self.address, authkey=self._authkey)

        def start(self):
            self.connection.send(('api', None))

        def send(self, msg_type, data):
            self.connection.send((msg_type, data))

    logger.debug('Connecting broker client...')
    broker_address_list = config['broker_address'].split(':')
    broker_address = (broker_address_list[0], int(broker_address_list[1]))
    broker_client = BrokerClient(broker_address, b'dtms')
    broker_client.start()
    logger.debug('Broker client connected')

    def write_segment_files(segment_file, segment_dest, segment_list_file, segment_list_dest):
        # emulate behaviour of ffmpeg so Emby picks up these files (in theory)

        segment_dest_file = open(segment_dest, mode='wb')

        # copy everything but last 10% which will be written later
        segment_file.seek(0, io.SEEK_END)
        file_size = segment_file.tell()
        last_chunk_size = int(round(file_size / 10))
        segment_file.seek(-1 * last_chunk_size, io.SEEK_END)
        first_chunk_size = segment_file.tell()

        logger.debug(f'segment size: {file_size}, first chunk size: {first_chunk_size}')

        segment_file.seek(0, io.SEEK_SET)
        segment_dest_file.write(segment_file.read(first_chunk_size))

        segment_list_tmp_dest = segment_list_dest + '.tmp'
        tmp_dest_file = open(segment_list_tmp_dest, mode='wb')
        shutil.copyfileobj(segment_list_file, tmp_dest_file, length=-1)
        tmp_dest_file.close()
        shutil.move(segment_list_tmp_dest, segment_list_dest)

        # copy last remaining unread bytes (read until EOF)
        segment_dest_file.write(segment_file.read(-1))

        segment_dest_file.close()

    # this is insecure
    @app.route('/transcode-sessions/<id_>/segments', methods=['POST'])
    def new_segment(id_):
        file_storage_multidict = request.files
        json_data = json.loads(request.form['json'])
        transcode_dir = json_data['master_transcoding_dir']

        logger.debug(f'Segment files received: {str(file_storage_multidict)}')
        logger.debug(f'transcode_dir: {transcode_dir}')

        segment_file_storage = file_storage_multidict['segment']
        segment_dest = os.path.join(transcode_dir, segment_file_storage.filename)
        segment_list_file_storage = file_storage_multidict['segment_list']
        segment_list_dest = os.path.join(transcode_dir, segment_list_file_storage.filename)

        write_segment_files(
            segment_file_storage.stream,
            segment_dest,
            segment_list_file_storage.stream,
            segment_list_dest
        )

        return '', HTTPStatus.CREATED

    @app.route('/transcode-sessions/<id_>/stdout', methods=['POST'])
    def new_stdout_data(id_):
        logger.info(f'stdout received for id: {id_}')

        json_data = request.get_json()
        message = json_data['output']

        broker_client.send('worker_output', (id_, 'stdout', message))

        return '', HTTPStatus.CREATED

    @app.route('/transcode-sessions/<id_>/stderr', methods=['POST'])
    def new_stderr_data(id_):
        logger.info(f'stderr received for id: {id_}')

        json_data = request.get_json()
        message = json_data['output']

        broker_client.send('worker_output', (id_, 'stderr', message))

        return '', HTTPStatus.CREATED

    @app.route('/transcode-sessions/<id_>', methods=['DELETE'])
    def session_finished(id_):
        logger.info(f'delete session request received for id: {id_}')

        broker_client.send('finish_session', (id_,))

        return '', HTTPStatus.ACCEPTED
        pass

    return app