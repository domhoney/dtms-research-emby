import argparse
import logging
from multiprocessing.connection import Listener
from threading import Thread, Lock

from dtms.master.messaging.config import get as get_config

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='dtms-worker-api', allow_abbrev=False)
    parser.add_argument('--bind', help='host:port')
    parser.add_argument('--log-file', help='Path to file for logging')
    parsed_args = parser.parse_args()
else:
    parsed_args = None

config = get_config(parsed_args)

logger = logging.getLogger('broker')
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler(config['log_file'], 'w')
file_handler.setLevel(logging.DEBUG)

formatter = logging.Formatter(
    fmt='%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
)

file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class BrokerListener(object):
    def __init__(self, address, authkey):
        self.address = address
        self._authkey = authkey
        self.listener = Listener(self.address, backlog=16, authkey=self._authkey)
        self.ffmpeg_connections = {}
        self.mutex = Lock()

    def start(self):
        self.connection_acceptor()

    def connection_acceptor(self):
        while True:
            try:
                connection = self.listener.accept()
            except OSError:
                continue
            Thread(target=self.handle_connection,
                   args=(connection,)).start()

    def handle_connection(self, connection):
        component, data = connection.recv()

        if component == 'transcoder':
            id_ = data
            with self.mutex:
                self.ffmpeg_connections[id_] = connection
                connection.send_bytes(b'TRANSCODER_ACK')
            logger.info(f'New transcoder connection id: {id_}')
        elif component == 'api':
            Thread(target=self._api_message_acceptor,
                   args=(connection,)).start()
            logger.info('New api connection created')
            logger.debug('New message acceptor thread started')

    def _api_message_acceptor(self, connection):
        while True:
            try:
                message = connection.recv()
                logger.debug(f'API message received, contents: {str(message)}')
                msg_type, data = message
                if msg_type == 'worker_output':
                    id_, method, output = data
                    if method == 'stderr' or 'stdout':
                        logger.info(f'{method} received from api, '
                                    f'forwarding to transcoder id: {id_}')
                        with self.mutex:
                            ffmpeg_connection = self.ffmpeg_connections.get(id_, None)
                            if ffmpeg_connection:
                                ffmpeg_connection.send((method, output))
                            else:
                                logger.error(f'connection not found, id {id_}')
                elif msg_type == 'finish_session':
                    id_, = data
                    logger.info(f'finish_session received from api, '
                                f'forwarding to transcoder id: {id_}')
                    with self.mutex:
                        ffmpeg_connection = self.ffmpeg_connections.pop(id_, None)
                        if ffmpeg_connection:
                            ffmpeg_connection.send(('finish',))
                        else:
                            logger.error(f'connection not found, id {id_}')

            except (EOFError, OSError):
                logger.error('API closed their end, closing socket...')
                connection.close()
                raise
            except:
                logger.exception('API message acceptor exception:', exc_info=1)
                raise


listener_address_list = config['bind'].split(':')
listener_address = (listener_address_list[0], int(listener_address_list[1]))
listener = BrokerListener(listener_address, b'dtms')
logger.info(f'Starting listener on address: {listener_address} ...')
listener.start()
