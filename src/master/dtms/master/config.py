import os.path
import anyconfig

DEFAULT_CONFIG = {
    'broker_address': '127.0.0.1:50000',
    'log_file': '/var/log/dtms/master/api.log'
}

conf_file = '/etc/dtms/master/api.yaml'


class Loader(object):
    _config = None

    def get(self, args):
        if not self._config:
            self._config = DEFAULT_CONFIG.copy()

            if os.path.exists(conf_file):
                file_config = anyconfig.load(conf_file)
                anyconfig.merge(self._config, file_config)

        if args:
            if args.broker_address:
                self._config['broker_address'] = args.broker_address
            if args.log_file:
                self._config['log_file'] = args.log_file

        return self._config


config_loader = Loader()


def get(args=None):
    return config_loader.get(args)
